FROM nginx:latest

COPY ./index.html /usr/share/nginx/html/
COPY ./404.html /usr/share/nginx/html/
